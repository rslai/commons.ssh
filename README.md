# commons.ssh

#### 项目介绍

目前开源ssh库，支持交互式命令和执行命令两种方式。

在这些开源库的基础上进行了封装，可以简单的实现一个连接完成多条命令的执行，并返回命令执行的结果。

目前封装了 jsch 库，但留有接口后边可以方便的集成其他 ssh 开源库。

#### 安装教程

pom中引入如下内容即可
```xml
   <dependency> 
        <groupId>com.gitee.rslai.commons</groupId> 
        <artifactId>ssh</artifactId> 
        <version>0.1-SNAPSHOT</version> 
   </dependency>
```

#### 使用说明

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost("127.1.1"); // ssh 服务器地址
        sshConfig.setUsername("sername"); // 登录 ssh 用户名
        sshConfig.setPassword("password"); // 登录 ssh 密码
        // sshConfig.setJumpServer(true); // 如果连接的是 JumpServer 要设置true
        SSH ssh = new Jsch(sshConfig);

        SSHResponse sshResponse = ssh.execute("pwd"); // 执行 linux 命令 pwd

        System.out.println("cmd: " + sshResponse.getCmd()); // 执行的 linux 命令，这里应该显示 pwd
        System.out.println("body: " + sshResponse.getBody()); // 执行 linux 命令的返回内容，如果在 root 目录这里显示 /root
        System.out.println("prompt: " + sshResponse.getPrompt()); // linux 命令提示行，例如：[root@iZ257d4pjcoZ ~]# 

        ssh.disconnect(); // 断开 ssh 连接

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)