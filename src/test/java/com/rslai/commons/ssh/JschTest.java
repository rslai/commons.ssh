package com.rslai.commons.ssh;

import com.jcraft.jsch.JSchException;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import static org.testng.Assert.*;

public class JschTest {
    String jumpServerHost = "host";
    String jumpServerUsername = "username";
    String jumpServerPassword = "password";

    String ptyHost = "host";
    String ptyUsername = "username";
    String ptyPassword = "password";
    /**
     * 测试只连linux服务器（pty）
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testPty() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(ptyHost);
        sshConfig.setUsername(ptyUsername);
        sshConfig.setPassword(ptyPassword);
        SSH ssh = new Jsch(sshConfig);

        ssh.execute("cd /");
        sshResponse = ssh.execute("pwd");
        assertEquals(sshResponse.getBody(), "/", "pwd校验失败");

        sshResponse = ssh.execute("");
        assertEquals(sshResponse.getBody(), "", "\"\"校验失败");

        sshResponse = ssh.execute("echo $LANG");
        assertEquals(sshResponse.getBody(), "zh_CN.UTF-8", "echo $LANG校验失败");

        sshResponse = ssh.execute("LANG=zh_CN.gb2312");
        assertEquals(sshResponse.getBody(), "", "echo $LANG校验失败");

        sshResponse = ssh.execute("ddd");
        assertEquals(sshResponse.getBody(), "-bash: ddd: δ�ҵ�����", "echo $LANG校验失败");

        ssh.disconnect(); // 断开连接
    }

    /**
     * 测试执行后台执行命令 nohup
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testPtyNohup() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(ptyHost);
        sshConfig.setUsername(ptyUsername);
        sshConfig.setPassword(ptyPassword);
        SSH ssh = new Jsch(sshConfig);


        sshResponse = ssh.execute("nohup ls -l > /dev/null 2>&1 &");
        assertTrue(sshResponse.getBody().contains("[1]"), "nohup ll > /dev/null 2>&1 &校验失败");

        sshResponse = ssh.execute("nohup ls -l > nohup.out 2>&1 &");
        assertTrue(sshResponse.getBody().contains("[1]"), "nohup ll > /dev/null 2>&1 &校验失败");

        sshResponse = ssh.execute("rm -f nohup.out");

        ssh.disconnect(); // 断开连接
    }

    /**
     * 测试通过JumpServer连接linux（39.106.43.2 国内服务器，速度相对快）
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testJumpServer39_106_43_2() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        String msms = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"debug\",\r\n" +
                "        \"logTags\": [\r\n" +
                "            \"info\",\r\n" +
                "            \"ice\",\r\n" +
                "            \"dtls\",\r\n" +
                "            \"rtp\",\r\n" +
                "            \"srtp\",\r\n" +
                "            \"rtcp\",\r\n" +
                "            \"rbe\",\r\n" +
                "            \"rtx\"\r\n" +
                "        ],\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"rtcIPv4\": \"172.17.0.176\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.17.0.176\",\r\n" +
                "        \"turnServerIp\": \"39.106.43.2\",\r\n" +
                "        \"turnServerPort\": 3478,\r\n" +
                "        \"turnServerUsername\": \"u1\",\r\n" +
                "        \"turnServerPassward\": \"u1\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10001,\r\n" +
                "    \"center\": \"http://192.168.95.2:8881/center\",\r\n" +
                "    \"ip\": \"39.106.43.2\",\r\n" +
                "    \"externalIP\": \"39.106.43.2\"\r\n" +
                "}";
        String msms_1 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"debug\",\r\n" +
                "        \"logTags\": [\r\n" +
                "            \"info\",\r\n" +
                "            \"ice\",\r\n" +
                "            \"dtls\",\r\n" +
                "            \"rtp\",\r\n" +
                "            \"srtp\",\r\n" +
                "            \"rtcp\",\r\n" +
                "            \"rbe\",\r\n" +
                "            \"rtx\"\r\n" +
                "        ],\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"rtcIPv4\": \"172.17.0.176\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.17.0.176\",\r\n" +
                "        \"turnServerIp\": \"39.106.43.2\",\r\n" +
                "        \"turnServerPort\": 3478,\r\n" +
                "        \"turnServerUsername\": \"u1\",\r\n" +
                "        \"turnServerPassward\": \"u1\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10002,\r\n" +
                "    \"center\": \"http://192.168.95.2:8881/center\",\r\n" +
                "    \"ip\": \"39.106.43.2\",\r\n" +
                "    \"externalIP\": \"39.106.43.2\"\r\n" +
                "}";
        String msms_2 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"debug\",\r\n" +
                "        \"logTags\": [\r\n" +
                "            \"info\",\r\n" +
                "            \"ice\",\r\n" +
                "            \"dtls\",\r\n" +
                "            \"rtp\",\r\n" +
                "            \"srtp\",\r\n" +
                "            \"rtcp\",\r\n" +
                "            \"rbe\",\r\n" +
                "            \"rtx\"\r\n" +
                "        ],\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"rtcIPv4\": \"172.17.0.176\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.17.0.176\",\r\n" +
                "        \"turnServerIp\": \"39.106.43.2\",\r\n" +
                "        \"turnServerPort\": 3478,\r\n" +
                "        \"turnServerUsername\": \"u1\",\r\n" +
                "        \"turnServerPassward\": \"u1\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10003,\r\n" +
                "    \"center\": \"http://192.168.95.2:8881/center\",\r\n" +
                "    \"ip\": \"39.106.43.2\",\r\n" +
                "    \"externalIP\": \"39.106.43.2\"\r\n" +
                "}";

        String msms_3 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"debug\",\r\n" +
                "        \"logTags\": [\r\n" +
                "            \"info\",\r\n" +
                "            \"ice\",\r\n" +
                "            \"dtls\",\r\n" +
                "            \"rtp\",\r\n" +
                "            \"srtp\",\r\n" +
                "            \"rtcp\",\r\n" +
                "            \"rbe\",\r\n" +
                "            \"rtx\"\r\n" +
                "        ],\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"rtcIPv4\": \"172.17.0.176\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.17.0.176\",\r\n" +
                "        \"turnServerIp\": \"39.106.43.2\",\r\n" +
                "        \"turnServerPort\": 3478,\r\n" +
                "        \"turnServerUsername\": \"u1\",\r\n" +
                "        \"turnServerPassward\": \"u1\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10004,\r\n" +
                "    \"center\": \"http://192.168.95.2:8881/center\",\r\n" +
                "    \"ip\": \"39.106.43.2\",\r\n" +
                "    \"externalIP\": \"39.106.43.2\"\r\n" +
                "}";

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(jumpServerHost);
        sshConfig.setUsername(jumpServerUsername);
        sshConfig.setPassword(jumpServerPassword);
        sshConfig.setPort(2223);
        sshConfig.setJumpServer(true);
        SSH ssh = new Jsch(sshConfig);

        //ssh.connect();
        ssh.execute("39.106.43.2");
        ssh.execute("echo off");
        ssh.execute("sudo su -");
        ssh.execute("ls -l");

        sshResponse = ssh.execute("cd /");
        assertEquals(sshResponse.getBody(), "", "cd /校验失败");

        ssh.execute("pwd");
        ssh.execute("ll");
        ssh.execute("cd /bin");
        ssh.execute("pwd");
        ssh.execute("exit");
        ssh.execute("ll");
        ssh.execute("cd /opt/vdeploy/projects/msms/msms/configs");
        ssh.execute("pwd");
        sshResponse = ssh.execute("cat config.json && echo \"\"");
        assertEquals(sshResponse.getBody(), msms, "cat msms 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_1/configs");
        sshResponse = ssh.execute("cat config.json && echo ");
        assertEquals(sshResponse.getBody(), msms_1, "cat msms_1 校验失败");

        sshResponse = ssh.execute("cat /opt/vdeploy/projects/msms/msms_1/configs/config.json && echo ");
        assertEquals(sshResponse.getBody(), msms_1, "cat msms_1 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_2/configs");
        sshResponse = ssh.execute("cat config.json && echo ");
        assertEquals(sshResponse.getBody(), msms_2, "cat msms_2 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_3/configs");
        sshResponse = ssh.execute("cat config.json && echo \"\"");
        assertEquals(sshResponse.getBody(), msms_3, "cat msms_3 校验失败");

        ssh.disconnect();
    }

    /**
     * 测试通过JumpServer连接linux（43.132.31.31 国外服务器，速度相对最慢）
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testJumpServer43_132_31_31() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        String msms = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.33\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.33\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10001,\r\n" +
                "    \"ip\": \"43.132.31.31\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.31.31:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.31.31\"\r\n" +
                "}";
        String msms_1 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.33\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.33\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10002,\r\n" +
                "    \"ip\": \"43.132.31.31\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.31.31:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.31.31\"\r\n" +
                "}";
        String msms_2 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.33\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.33\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10003,\r\n" +
                "    \"ip\": \"43.132.31.31\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.31.31:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.31.31\"\r\n" +
                "}";
        String msms_3 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.33\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.33\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10004,\r\n" +
                "    \"ip\": \"43.132.31.31\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.31.31:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.31.31\"\r\n" +
                "}";

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(jumpServerHost);
        sshConfig.setUsername(jumpServerUsername);
        sshConfig.setPassword(jumpServerPassword);
        sshConfig.setPort(2223);
        sshConfig.setJumpServer(true);
        SSH ssh = new Jsch(sshConfig);

        ssh.execute("43.132.31.31");
        ssh.execute("echo off");
        ssh.execute("sudo su -");
        ssh.execute("ls -l");
        ssh.execute("cd /");
        ssh.execute("pwd");
        ssh.execute("ll");
        ssh.execute("cd /bin");
        ssh.execute("pwd");
        ssh.execute("exit");
        ssh.execute("ll");
        ssh.execute("cd /opt/vdeploy/projects/msms/msms/configs");
        ssh.execute("pwd");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms, "cat msms 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_1/configs");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms_1, "cat msms_1 校验失败");

        sshResponse = ssh.execute("cat /opt/vdeploy/projects/msms/msms_1/configs/config.json");
        assertEquals(sshResponse.getBody(), msms_1, "cat msms_1 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_2/configs");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms_2, "cat msms_2 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_3/configs");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms_3, "cat msms_3 校验失败");

        ssh.disconnect();
    }

    /**
     * 测试通过JumpServer连接linux（43.132.29.124 国外服务器，速度慢容易返回乱字符）
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testJumpServer43_132_29_124() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        String msms = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.41\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.41\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10001,\r\n" +
                "    \"ip\": \"43.132.29.124\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.29.124:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.29.124\"\r\n" +
                "}";
        String msms_1 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.41\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.41\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10002,\r\n" +
                "    \"ip\": \"43.132.29.124\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.29.124:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.29.124\"\r\n" +
                "}";
        String msms_2 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.41\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.41\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10003,\r\n" +
                "    \"ip\": \"43.132.29.124\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.29.124:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.29.124\"\r\n" +
                "}";
        String msms_3 = "{\r\n" +
                "    \"jwt\": {\r\n" +
                "        \"secret\": \"vloud\",\r\n" +
                "        \"expires\": 3600\r\n" +
                "    },\r\n" +
                "    \"type\": \"msms\",\r\n" +
                "    \"reconnect_interval\": 1000,\r\n" +
                "    \"timeout\": 5000,\r\n" +
                "    \"mediasoup\": {\r\n" +
                "        \"logLevel\": \"warn\",\r\n" +
                "        \"logTags\": \"\",\r\n" +
                "        \"rtx\": true,\r\n" +
                "        \"rtcMinPort\": 10000,\r\n" +
                "        \"rtcMaxPort\": 30000,\r\n" +
                "        \"qsapSoftKey\": \"configs/SoftKeyFile/SoftKeyServer.dk\",\r\n" +
                "        \"qsapOwnerKey\": \"0123456789ABCDEF\",\r\n" +
                "        \"qsapPin\": \"00000000\",\r\n" +
                "        \"qsapAsServer\": false,\r\n" +
                "        \"qsapAsClient\": true,\r\n" +
                "        \"turnServerIp\": \"43.132.20.227\",\r\n" +
                "        \"rtcIPv4\": \"172.19.16.41\",\r\n" +
                "        \"rtcAnnouncedIPv4\": \"172.19.16.41\"\r\n" +
                "    },\r\n" +
                "    \"port\": 10004,\r\n" +
                "    \"ip\": \"43.132.29.124\",\r\n" +
                "    \"responseTimeThreshold\": 2200,\r\n" +
                "    \"rtVarianceThreshold\": 500,\r\n" +
                "    \"kafka_addr\": \"172.16.0.2:19092,172.16.0.2:19093,172.16.0.2:19094\",\r\n" +
                "    \"kafka_pwd\": \"\",\r\n" +
                "    \"kafka_user\": \"\",\r\n" +
                "    \"kafka_topic\": \"topic-msms-stream-stat-online\",\r\n" +
                "    \"sentinelCenter\": \"http://43.132.29.124:9802\",\r\n" +
                "    \"center\": \"http://43.132.20.227:8881/center\",\r\n" +
                "    \"stat_interval\": 5000,\r\n" +
                "    \"consul\": {\r\n" +
                "        \"host\": \"123.57.230.32\",\r\n" +
                "        \"port\": 8500,\r\n" +
                "        \"prefix\": \"hw.puller.rtcs.bjy\"\r\n" +
                "    },\r\n" +
                "    \"externalIP\": \"43.132.29.124\"\r\n" +
                "}";

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(jumpServerHost);
        sshConfig.setUsername(jumpServerUsername);
        sshConfig.setPassword(jumpServerPassword);
        sshConfig.setPort(2223);
        sshConfig.setJumpServer(true);
        SSH ssh = new Jsch(sshConfig);

        ssh.execute("43.132.29.124");
        ssh.execute("echo off");
        ssh.execute("sudo su -");
        ssh.execute("ls -l");
        ssh.execute("cd /");
        ssh.execute("pwd");
        ssh.execute("ll");
        ssh.execute("cd /bin");
        ssh.execute("pwd");
        ssh.execute("exit");
        ssh.execute("ll");
        ssh.execute("cd /opt/vdeploy/projects/msms/msms/configs");
        ssh.execute("pwd");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms, "cat msms 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_1/configs");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms_1, "cat msms_1 校验失败");

        sshResponse = ssh.execute("cat /opt/vdeploy/projects/msms/msms_1/configs/config.json");
        assertEquals(sshResponse.getBody(), msms_1, "cat msms_1 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_2/configs");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms_2, "cat msms_2 校验失败");

        ssh.execute("cd /opt/vdeploy/projects/msms/msms_3/configs");
        sshResponse = ssh.execute("cat config.json");
        assertEquals(sshResponse.getBody(), msms_3, "cat msms_3 校验失败");

        ssh.disconnect();
    }

    /**
     * 检查命令行会被加\r的问题，这台服务器39.106.43.2不会加\r
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testSSH_1() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(jumpServerHost);
        sshConfig.setUsername(jumpServerUsername);
        sshConfig.setPassword(jumpServerPassword);
        sshConfig.setPort(2223);
        sshConfig.setJumpServer(true);
        SSH ssh = new Jsch(sshConfig);

        ssh.execute("39.106.43.2");
        sshResponse = ssh.execute("cd /opt/vdeploy/projects/msms/msms/configs");
        assertEquals(sshResponse.getCmd(), "cd /opt/vdeploy/projects/msms/msms/configs", "cd /opt/vdeploy/projects/msms/msms/configs 校验失败");

        ssh.disconnect();
    }

    /**
     * 检查命令行会被加\r的问题，这台服务器容易有这个问题43.132.31.31
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testSSH_2() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(jumpServerHost);
        sshConfig.setUsername(jumpServerUsername);
        sshConfig.setPassword(jumpServerPassword);
        sshConfig.setPort(2223);
        sshConfig.setJumpServer(true);
        SSH ssh = new Jsch(sshConfig);

        ssh.execute("43.132.31.31");
        sshResponse = ssh.execute("cd /opt/vdeploy/projects/msms/msms/configs");
        assertEquals(sshResponse.getCmd(), "cd /opt/vdeploy/projects/msms/msms/configs", "cd /opt/vdeploy/projects/msms/msms/configs 校验失败");

        ssh.disconnect();
    }

    /**
     * 检查命令行会被加\r的问题，这台服务器容易有这个问题43.132.29.124
     * @throws JSchException
     * @throws IOException
     * @throws TimeoutException
     */
    @Test
    public void testSSH_3() throws JSchException, IOException, TimeoutException {
        if (jumpServerUsername.equals("username")) {
            return;
        }

        SSHResponse sshResponse;

        SSHConfig sshConfig = new SSHConfig();
        sshConfig.setHost(jumpServerHost);
        sshConfig.setUsername(jumpServerUsername);
        sshConfig.setPassword(jumpServerPassword);
        sshConfig.setPort(2223);
        sshConfig.setJumpServer(true);
        SSH ssh = new Jsch(sshConfig);

        ssh.execute("43.132.29.124");
        sshResponse = ssh.execute("cd /opt/vdeploy/projects/msms/msms/configs");
        assertEquals(sshResponse.getCmd(), "cd /opt/vdeploy/projects/msms/msms/configs", "cd /opt/vdeploy/projects/msms/msms/configs 校验失败");

        ssh.disconnect();
    }

}