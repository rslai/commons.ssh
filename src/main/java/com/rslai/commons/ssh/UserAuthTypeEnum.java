package com.rslai.commons.ssh;

/**
 * 用户验证方式
 */
public enum UserAuthTypeEnum {
    PUBKEY, // 公钥验证
    PASSWORD; // 用户名密码验证
}
