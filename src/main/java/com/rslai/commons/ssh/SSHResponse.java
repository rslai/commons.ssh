package com.rslai.commons.ssh;

public interface SSHResponse {

    /**
     * SSHResponse信息是否处理完成
     * @return
     */
    public boolean isDone();

    /**
     * 返回用户输入的cmd
     * @return
     */
    public String getCmd();

    /**
     * 返回服务器执行cmd后响应的内容
     * @return
     */
    public String getBody();

    /**
     * 返回命令行提示符行的内容
     * @return
     */
    public String getPrompt();

    /**
     * 添加放回信息，从ssh输出流读入一组数据后添加一次
     * @param bytes
     * @param length
     */
    public void addResponse(byte[] bytes, int length);

}
