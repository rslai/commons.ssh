package com.rslai.commons.ssh;

import java.io.UnsupportedEncodingException;

public class util {
    static byte[] str2byte(String str, String encoding) {
        if (str == null) {
            return null;
        } else {
            try {
                return str.getBytes(encoding);
            } catch (UnsupportedEncodingException var3) {
                return str.getBytes();
            }
        }
    }

    static byte[] str2byte(String str) {
        return str2byte(str, "UTF-8");
    }

    static String byte2str(byte[] str, String encoding) {
        return byte2str(str, 0, str.length, encoding);
    }

    static String byte2str(byte[] str, int s, int l, String encoding) {
        try {
            return new String(str, s, l, encoding);
        } catch (UnsupportedEncodingException var5) {
            return new String(str, s, l);
        }
    }

    static String byte2str(byte[] str) {
        return byte2str(str, 0, str.length, "UTF-8");
    }

    static String byte2str(byte[] str, int s, int l) {
        return byte2str(str, s, l, "UTF-8");
    }

    static String toHex(byte[] str, int s, int l) {
        StringBuffer sb = new StringBuffer();

        for (int i = s; i < l; ++i) {
            String foo = Integer.toHexString(str[i] & 255);
            sb.append("0x" + (foo.length() == 1 ? "0" : "") + foo);
            if (i + 1 < str.length) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    static String toHex(byte[] str) {
        return toHex(str, 0, str.length);
    }

    /**
     * ascii码对照表
     */
    private String asciiTable[] = new String[]{
            "空字符(NUL)", "标题起始(Ctrl/A)", "文本起始(Ctrl/B)", "文本结束(Ctrl/C)", "EOT传输结束(Ctrl/D)", "询问(Ctrl/E)", "认可(Ctrl/F)", "铃(Ctrl/G)", "退格(Ctrl/H)", "水平制表栏(Ctrl/I)", "换行(\\n)", // 0-10
            "垂直制表栏(Ctrl/K)", "换页(Ctrl/L)", "回车(\\r)", "移出(Ctrl/N)", "移入(Ctrl/O)", "数据链接丢失(Ctrl/P)", "设备控制1(Ctrl/Q)", "设备控制2(Ctrl/R)", "设备控制3(Ctrl/S)", "设备控制4(Ctrl/T)", // 11-20
            "否定接受(Ctrl/U)", "同步闲置符(Ctrl/V)", "传输块结束(Ctrl/W)", "取消(Ctrl/X)", "媒体结束(Ctrl/Y)", "替换(Ctrl/Z)", "换码符(ESC)", "文件分隔符", "组分隔符", "记录分隔符", // 21-30
            "单位分隔符", " ", "!", "\"", "#", "$", "%", "&", "'", "(", // 31-40
            ")", "*", "+", ",", "--", ".", "/", "0", "1", "2", // 41-50
            "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", // 51-60
            "=", ">", "?", "@", "A", "B", "C", "D", "E", "F", // 61-70
            "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", // 71-80
            "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", // 81-90
            "[", "\\", "]", "^", "_", "`", "a", "b", "c", "d", // 91-100
            "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", // 101-110
            "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", // 111-120
            "y", "z", "{", "|", "}", "~", "擦掉(DELETE)", "保留", "保留", "保留", // 121-130
            "保留", "索引", "下一行", "被选区域起始", "被选区域结束", "水平制表符集", "对齐的水平制表符集", "垂直制表符集", "部分行向下", "部分行向上", // 131-140
            "反向索引", "单移2", "单移3", "设备控制字符串", "专用1", "专用2", "设置传输状态", "取消字符", "消息等待", "保护区起始", // 141-150
            "保护区结束", "保留", "保留", "保留", "控制序列引导符", "字符串终止符", "操作系统命令", "秘密消息", "应用程序", "保留", // 151-160
            "¡", "¢", "£", "保留", "¥", "保留", "§", "¤", "©", "ª", // 161-170
            "«", "保留", "保留", "保留", "保留", "°", "±", "²", "³", "保留", // 171-180
            "µ", "¶", "·", "保留", "¹", "º", "»", "¼", "½", "保留", // 181-190
            "¿", "À", "Á", "Â", "Ã", "Ä", "Å", "Æ", "Ç", "È", // 191-200
            "É", "Ê", "Ë", "Ì", "Í", "Î", "Ï", "保留", "Ñ", "Ò", // 201-210
            "Ó", "Ô", "Õ", "Ö", "OE", "Ø", "Ù", "Ú", "Û", "Ü", // 211-220
            "Y", "保留", "ß", "à", "á", "â", "ã", "ä", "å", "æ", // 221-230
            "ç", "è", "é", "ê", "ë", "ì", "í", "î", "ï", "保留", // 231-240
            "ñ", "ò", "ó", "ô", "õ", "ö", "oe", "ø", "ù", "ú", // 241-250
            "û", "ü", "ÿ", "保留", "保留" // 251-255
    };

    /**
     * 将 byte 数组转成 ascii 内容
     * 例如：将 [97, 13, 10] 转成 97[a] 13[\r] 10[\n]
     * @param bytes
     * @param start
     * @param length
     */
    public String toAsciiString(byte[] bytes, int start, int length) {
        StringBuffer sb = new StringBuffer();

        for (int i = start; i < length; i++) {
            int bInt = bytes[i] & 0xFF;
            sb.append(bInt + "[" + asciiTable[bInt] + "]");

            if (i + 1 < length) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    /**
     * 将 byte 数组转成 ascii 内容
     * 例如：将 [97, 13, 10] 转成 97[a] 13[\r] 10[\n]
     * @param bytes
     * @return
     */
    public String toAsciiString(byte[] bytes) {
        return toAsciiString(bytes, 0, bytes.length);
    }

    /**
     * 将 byte 数组转成 ascii 内容
     * 例如：将 [97, 13, 10] 转成 97[a] 13[\r] 10[\n]
     * @param bytes
     * @param length
     * @return
     */
    public String toAsciiString(byte[] bytes, int length) {
        return toAsciiString(bytes, 0, length);
    }

}
