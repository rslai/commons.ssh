package com.rslai.commons.ssh;

/**
 * 待连接的ssh操作系统类型
 */
public enum OSTypeEnum {
    WINDOWS, // windows系统
    LINUX, // linux系统
    MACOS; // mac os系统
}
