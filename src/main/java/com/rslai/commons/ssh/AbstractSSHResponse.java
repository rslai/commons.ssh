package com.rslai.commons.ssh;

public abstract class AbstractSSHResponse {
    protected SSHConfig sshConfig; // ssh配置信息
    protected boolean isConnect = true; // 是否是connect，true：当是JumpServer 读最后是 "> " 认为结束，否则读最后是 "# " 或 "$ " 认为结束

    protected boolean done = false; // 是否读到结尾，true：读完，false：没读完
    protected String cmd = ""; // 命令行，也就是用户输入的内容
    protected String body = ""; // 命令执行返回的结果
    protected String prompt = ""; // 命令执行后系统补上的提示行

    /**
     * SSHResponse信息是否处理完成
     * @return
     */
    public boolean isDone() {
        return done;
    }

    /**
     * 返回用户输入的cmd
     * @return
     */
    public String getCmd() {
        return cmd;
    }

    /**
     * 返回服务器执行cmd后响应的内容
     * @return
     */
    public String getBody() {
        return body;
    }

    /**
     * 返回命令行提示符行的内容
     * @return
     */
    public String getPrompt() {
        return prompt;
    }

}
