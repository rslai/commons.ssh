package com.rslai.commons.ssh;

public class SSHConfig {
    protected String host; // ssh 服务器地址
    protected String username; // ssh 用户名
    protected String password; // ssh 密码
    protected int port = 22; // ssh端口号，默认22

    private UserAuthTypeEnum userAuthType = UserAuthTypeEnum.PASSWORD; // 用户验证方式，默认用户名、密码验证
    private StrictHostKeyCheckingTypeEnum strictHostKeyCheckingType = StrictHostKeyCheckingTypeEnum.NO; // 设置OpenSSH公钥核对方式，默认 no，没有提示自动保存key到known_hosts
    private String charset = "UTF-8"; // 设置字符集，默认UTF-8

    private int connectTimeout = 60000; // 连接超时时间
    private int readTimeout = 30000; // 读数据超时时间

    private boolean isJumpServer = false; // connect 连接的是否是JumpServer，默认不是
    private char jumpServerPrompt = '>'; // JumpServer连接提示符，显示出来后连接成功过
    private OSTypeEnum osType = OSTypeEnum.LINUX; // 待连接的ssh操作系统类型，默认linux
    private char rootPrompt = '#'; // 超级管理员root命令行提示符，显示出来后命令执行成功并等待执行下一个命令
    private char userPrompt = '$'; // 普通用户命令行提示符，显示出来后命令执行成功并等待执行下一个命令

    /**
     * 返回 ssh 服务器地址
     * @return
     */
    public String getHost() {
        return host;
    }

    /**
     * 设置 ssh 服务器地址
     * @param host
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * 返回 ssh 用户名
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置 ssh 用户名
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 返回 ssh 密码
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置 ssh 密码
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
        this.setUserAuthType(UserAuthTypeEnum.PASSWORD); // 如果调用用户名密码构造方法，认证方式默认设置为用户名、密码验证
    }

    /**
     * 返回 ssh端口号，默认22
     * @return
     */
    public int getPort() {
        return port;
    }

    /**
     * 设置 ssh端口号，默认22
     * @param port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * 返回 用户验证方式，默认用户名、密码验证
     * @return
     */
    public UserAuthTypeEnum getUserAuthType() {
        return userAuthType;
    }

    /**
     * 设置 用户验证方式，默认用户名、密码验证
     * @param userAuthType
     */
    public void setUserAuthType(UserAuthTypeEnum userAuthType) {
        this.userAuthType = userAuthType;
    }

    /**
     * 返回 OpenSSH公钥核对方式，默认 no，没有提示自动保存key到known_hosts
     * @return
     */
    public StrictHostKeyCheckingTypeEnum getStrictHostKeyChecking() {
        return strictHostKeyCheckingType;
    }

    /**
     * 设置OpenSSH公钥核对方式，默认 no，没有提示自动保存key到known_hosts
     * @param strictHostKeyCheckingType
     */
    public void setStrictHostKeyChecking(StrictHostKeyCheckingTypeEnum strictHostKeyCheckingType) {
        this.strictHostKeyCheckingType = strictHostKeyCheckingType;
    }

    /**
     * 返回 字符集，默认UTF-8
     * @return
     */
    public String getCharset() {
        return charset;
    }

    /**
     * 设置字符集，默认 UTF-8，可以设置 GB2312 、 UTF-8
     * @param charset
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * 返回 连接超时时间
     * @return
     */
    public int getConnectTimeout() {
        return connectTimeout;
    }

    /**
     * 设置 连接超时时间
     * @param connectTimeout
     */
    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    /**
     * 返回 读数据超时时间
     * @return
     */
    public int getReadTimeout() {
        return readTimeout;
    }

    /**
     * 设置 读数据超时时间
     * @param readTimeout
     */
    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    /**
     * 返回 连接的是否是jumpServer跳板机。true:跳拌机， false:不是跳板机
     * @return
     */
    public boolean isJumpServer() {
        return isJumpServer;
    }

    /**
     * 设置连接的是否是jumpServer跳板机。true:跳拌机， false:不是跳板机
     * @param jumpServer
     */
    public void setJumpServer(boolean jumpServer) {
        isJumpServer = jumpServer;
    }

    /**
     * 返回 JumpServer连接提示符，显示出来后连接成功过
     * @return
     */
    public char getJumpServerPrompt() {
        return jumpServerPrompt;
    }

    /**
     * 设置 JumpServer连接提示符，显示出来后连接成功过
     * @param jumpServerPrompt
     */
    public void setJumpServerPrompt(char jumpServerPrompt) {
        this.jumpServerPrompt = jumpServerPrompt;
    }

    /**
     * 返回 待连接的ssh操作系统类型，默认linux
     * @return
     */
    public OSTypeEnum getOsType() {
        return osType;
    }

    /**
     * 设置待连接的操作系统类型
     * @param osType
     */
    public void setOsType(OSTypeEnum osType) {
        this.osType = osType;

        switch (osType) {
            case WINDOWS:
                // windows 系统不知道能不能用，需要调试
                break;
            case MACOS:
                this.rootPrompt = '#';
                this.userPrompt = '%';
                break;
            default:
                this.rootPrompt = '#';
                this.userPrompt = '$';
        }
    }

    /**
     * 超级管理员root命令行提示符，显示出来后命令执行成功并等待执行下一个命令
     * @return
     */
    public char getRootPrompt() {
        return rootPrompt;
    }

    /**
     * 普通用户命令行提示符，显示出来后命令执行成功并等待执行下一个命令
     * @return
     */
    public char getUserPrompt() {
        return userPrompt;
    }

    @Override
    public String toString() {
        return "SSHConfig{" +
                "host='" + host + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", port=" + port +
                ", userAuthType=" + userAuthType +
                ", strictHostKeyCheckingType=" + strictHostKeyCheckingType +
                ", charset='" + charset + '\'' +
                ", connectTimeout=" + connectTimeout +
                ", readTimeout=" + readTimeout +
                ", isJumpServer=" + isJumpServer +
                ", jumpServerPrompt=" + jumpServerPrompt +
                ", osType=" + osType +
                ", rootPrompt=" + rootPrompt +
                ", userPrompt=" + userPrompt +
                '}';
    }
}
