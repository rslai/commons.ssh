package com.rslai.commons.ssh;

/**
 * OpenSSH 公钥核对方式。当前访问的计算机公钥是不是在~/.ssh/know_hosts中。
 */
public enum StrictHostKeyCheckingTypeEnum {
    YES, // 最安全的级别，如果key不匹配，拒绝连接，不会提示详细信息
    NO, // 最不安全的级别，当然也没有那么多烦人的提示了，如果连接server的key在本地不存在，那么就自动添加到文件中（默认是known_hosts），并且给出一个警告
    ASK; // 默认的级别，就是没有key给出提示。如果连接和key不匹配，给出提示并拒绝登录。
}
