package com.rslai.commons.ssh;

import com.jcraft.jsch.JSchException;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public interface SSH {

    /**
     * 执行一条命令
     * @param command 命令内容，不能包含\r\n
     * @return 返回执行的结果
     * @throws IOException
     * @throws JSchException
     * @throws TimeoutException
     */
    public SSHResponse execute(String command) throws IOException, JSchException, TimeoutException;

    /**
     * 断开ssh服务器，如果不断开io会一直阻塞
     */
    public void disconnect() throws TimeoutException;

}
